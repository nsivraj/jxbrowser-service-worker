self.addEventListener('message', function(event) {
  var promise = self.clients.matchAll()
    .then(function(clientList) {
      clientList.forEach(function(client) {
        client.postMessage(event.data);
      });
    });

  if (event.waitUntil) {
    event.waitUntil(promise);
  }
});

self.addEventListener('activate', function(event) {
  event.waitUntil(self.clients.claim());
});

